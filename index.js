const Hapi = require('@hapi/hapi');
const Path = require('path')



const server = Hapi.Server({
  port: 7000,
  host:'localhost',
}); 

const io = require('socket.io')(server.listener);
const init = async()=>{
  await server.register(require('inert'))
  server.route({
    method:'GET',
    path : '/{param*}',
    handler:{
      directory:{
        path:(__dirname,'./src')
      }
    }
  })
  var Users ;
    var io = require('socket.io')(server.listener);

io.on('connection', function (socket) {
  var addedUser = false;

  socket.on('new message', (data) => {
      socket.broadcast.emit('new message', {
      username: socket.username,
      message: data
      })
  })
  socket.on('add user', (username) => {
    if (addedUser) return;

    
    ++Users;
    addedUser = true;
    socket.emit('login', {
    Users: Users
    })
    
    socket.broadcast.emit('user joined', {
    username: socket.username,
    Users: Users
    })
})

// when the client emits 'typing', we broadcast it to others
socket.on('typing', () => {
    socket.broadcast.emit('typing', {
    username: socket.username
    })
})

// when the client emits 'stop typing', we broadcast it to others
socket.on('stop typing', () => {
    socket.broadcast.emit('stop typing', {
    username: socket.username
    })
})


}) 
  server.start()
  console.log('Server running on %s', server.info.uri);
}


process.on('unhandledRejection', (err) => {

  console.log(err);
  process.exit(1);
})

init();
